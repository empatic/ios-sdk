# CorePulsar

[![CI Status](http://img.shields.io/travis/carolina.manterola/CorePulsar.svg?style=flat)](https://travis-ci.org/carolina.manterola/CorePulsar)
[![Version](https://img.shields.io/cocoapods/v/CorePulsar.svg?style=flat)](http://cocoapods.org/pods/CorePulsar)
[![License](https://img.shields.io/cocoapods/l/CorePulsar.svg?style=flat)](http://cocoapods.org/pods/CorePulsar)
[![Platform](https://img.shields.io/cocoapods/p/CorePulsar.svg?style=flat)](http://cocoapods.org/pods/CorePulsar)

## Installation

CorePulsar is available through Empatic's Bitbucket repository. To install
it, simply add the following line to your Podfile:

```ruby
pod "CorePulsar", :git => 'https://bitbucket.org/empatic/ios-sdk'
```

## Author

Empatic, 2015. All right reserved.

## License

CorePulsar is distributed under a commercial license. See LICENSE file for more info.
