Pod::Spec.new do |s|
  s.name             = 'CorePulsar'
  s.version          = '2.1'
  s.summary          = 'Empatic Pulsar Proximity framework for iBeacon support'
  s.description      = <<-DESC
CorePulsar is Empatic's solution to proximity marketing adding support for iBeacon
                       DESC
  s.homepage         = 'https://bitbucket.org/empatic/'
  s.license          = { :type => 'Copyright',
    :text => <<-LICENSE
Copyright 2015 Empatic. All rights reserved.
                LICENSE
    }
  s.author           = { 'Empatic' => 'contact@empatic.mx' }
  s.source           = { :git => 'https://bitbucket.org/empatic/core-pulsar-ios-pod.git', :tag => s.version.to_s }
  s.platform     = :ios
  s.ios.deployment_target = '8.2'

  s.source_files = '*.h'
  s.preserve_paths = 'CorePulsar.framework'
  s.vendored_frameworks = 'CorePulsar.framework'

  s.frameworks = 'UIKit', 'Foundation', 'CoreData', 'CoreTelephony', 'CoreLocation', 'AVFoundation'
  s.requires_arc = true
  s.xcconfig  =  { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/CorePulsarFramework"',
 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Headers/CorePulsarFramework"' }

  s.resource = 'CorePulsar.framework'
end
