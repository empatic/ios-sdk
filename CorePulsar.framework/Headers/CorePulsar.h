//
//  CorePulsar.h
//  CorePulsar
//
//  Created by Carolina on 3/20/15.
//  Copyright (c) 2015 empatic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

//! Project version number for CorePulsar.
FOUNDATION_EXPORT double CorePulsarVersionNumber;

//! Project version string for CorePulsar.
FOUNDATION_EXPORT const unsigned char CorePulsarVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CorePulsar/PublicHeader.h>


